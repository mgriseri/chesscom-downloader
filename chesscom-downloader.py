#!/usr/bin/env python3

from datetime import date

import json

import os
import os.path

from time import sleep

import urllib.request
import urllib.parse

import PySimpleGUI as GUI

home = os.path.expanduser("~")
os.chdir(home)

today = date.today().strftime("%Y.%m.%d")

GUI.theme("SystemDefaultForReal")

layout = [
    [GUI.Text("Username:"), GUI.InputText(key="text_username")],
    [GUI.Text("Progress (in %):"), GUI.Text("0", key="text_progress")],
    [GUI.Button("Ok"), GUI.Button("Cancel")]
]

window = GUI.Window("chesscom-downloader", layout)

progress = 0

while True:
    event, values = window.read()
    if event == GUI.WIN_CLOSED or event == "Cancel":
        break

    username = window["text_username"].get()

    if username and event == "Ok":

        url = "https://api.chess.com/pub/player/" + username + "/games/archives"
        response = urllib.request.urlopen(url).read().decode('utf-8')

        monthly_game_list = json.loads(response)["archives"]
        monthly_game_list_downloadable = [url + "/pgn" for url in monthly_game_list]
        monthly_game_list_downloadable.reverse()

        all_games_as_pgn = ""

        for month_games_url in monthly_game_list_downloadable:
            month_games_as_pgn = urllib.request.urlopen(month_games_url).read().decode('utf-8')
            all_games_as_pgn = all_games_as_pgn + month_games_as_pgn
            print("Downloading: " + month_games_url)

            progress = progress + (100 * (1 / len(monthly_game_list_downloadable)))
            window["text_progress"].update(str(round(progress)))
            window.refresh()

            sleep(0.5)

        filename = f"chesscom-games-{username}-{today}.pgn"

        with open(filename, "w") as f:
            print(f"Creating '{filename}' file")
            f.write(all_games_as_pgn)

        GUI.popup(f"The file '{filename}' has been created in home directory!")

        break

window.close()
